from flask import Flask
from database.db import initialize_db
from flask_bcrypt import Bcrypt
from flask_restful import Api
from flask_jwt_extended import JWTManager
from resources.errors import errors
from flask_mail import Mail
from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')
mail = Mail(app)


from resources.routes import initialize_routes

initialize_db(app)
api = Api(app, errors=errors)
initialize_routes(api)
jwt = JWTManager(app)
bcryptapi = Bcrypt(app)
mail = Mail(app)
