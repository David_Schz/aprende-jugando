import random, time

hang = ["""
H A N G M A N - Fruit Edition

   +---+
   |   |
       |
       |
       |
       |
=========""", """
H A N G M A N - Fruits Edition

  +---+
  |   |
  O   |
      |
      |
      |
=========""", """
H A N G M A N - Fruits Edition

  +---+
  |   |
  O   |
  |   |
      |
      |
=========""", """
H A N G M A N - Fruits Edition

  +---+
  |   |
  O   |
 /|   |
      |
      |
=========""", """
H A N G M A N - Fruits Edition

  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========""", """
H A N G M A N - Fruits Edition

  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========""", """
H A N G M A N - Fruits Edition

  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
========="""]


def get_random_word():
    words = ['apple']

    word = random.choice(words)
    return word


def display_board(hang, missed_letters, 
                correct_letters, secret_word):
    
    print(hang[len(missed_letters)])
    print()

    print('Missed Letters: ', end = ' ')
    for letter in missed_letters:
        print(letter, end=' ')

    blanks = '_' * len(secret_word)
    print("\n")
    for i in range(len(secret_word)): # replace blanks with correctly guessed letters
        if secret_word[i] in correct_letters:
            blanks = blanks[:i] + secret_word[i] + blanks[i+1:]

    for letter in blanks: # show the secret word with spaces in between each letter
        print(letter, end=' ')
        


def get_guess(already_guessed):
    while True:
        print("\n")
        guess = input('Guess a letter: ')
        guess = guess.lower()
        if len(guess) != 1:
            print('Please enter a single letter.')
        elif guess in already_guessed:
            print('You have already guessed that letter. Choose again.')
        elif guess not in 'abcdefghijklmnopqrstuvwxyz':
            print('Please enter a LETTER.')
        else:
            return guess


def playAgain():
    return input("\nDo you want to play again? ").lower().startswith('y')


missed_letters = ''
correct_letters = ''
secret_word = get_random_word()
game_is_done = False

while True:
    display_board(hang, missed_letters, correct_letters, secret_word)

    guess = get_guess(missed_letters + correct_letters)

    if guess in secret_word:
        correct_letters = correct_letters + guess

        found_all_letters = True
        for i in range(len(secret_word)):
            if secret_word[i] not in correct_letters:
                found_all_letters = False
                break
        if found_all_letters:
            print('\nYes! The secret word is "' +
                  secret_word + '"! You have won!')
            game_is_done = True
    else:
        missed_letters = missed_letters + guess

        if len(missed_letters) == len(hang) - 1:
            display_board(hang, missed_letters,
                         correct_letters, secret_word)
            print('You have run out of guesses!\nAfter ' + str(len(missed_letters)) + ' missed guesses and ' +
                  str(len(correct_letters)) + ' correct guesses, the word was "' + secret_word + '"')
            game_is_done = True

    if game_is_done:
        if playAgain():
            missed_letters = ''
            correct_letters = ''
            game_is_done = False
            secret_word = get_random_word()
        else:
            break

