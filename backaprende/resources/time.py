from flask_restful import Resource 
import time

class get_current_time(Resource):
    def get(self):
        return {'time': time.time()}
