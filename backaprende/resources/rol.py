from flask import Response, request
from database.models import Rol
from flask_restful import Resource


class RolCreatorApi(Resource):
    def post(self):
        body = request.get_json()
        rol = Rol(**body)
        rol.save()
        id = rol.id
        return {'idRol': str(id)}, 200