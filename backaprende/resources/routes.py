from .auth import SignupApi, LoginApi
from .rol import RolCreatorApi
from .reset_password import ForgotPassword, ResetPassword
from .time import get_current_time

def initialize_routes(api):
    api.add_resource(SignupApi, '/api/auth/signup')
    api.add_resource(LoginApi, '/api/auth/login')

    api.add_resource(RolCreatorApi, '/api/rol/rolcreator')

    #Reset Password
    api.add_resource(ForgotPassword, '/api/auth/forgot')
    api.add_resource(ResetPassword, '/api/auth/reset')

    #Frontend test
    api.add_resource(get_current_time, '/api/time')