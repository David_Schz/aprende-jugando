import json


from tests.BaseCase import BaseCase

    
class TestUserLogin(BaseCase):

    def test_successful_login(self):
        #Given
        email = "paurakh011@gmail.com"
        password = "mycoolpassword"
        payload = json.dumps({
            "nickname": "unit",
            "email": "mail@mail.com",
            "password": "mycoolpassword",
            "rol": "6096f7c88acf62d2c5e19f8a",
            "is_federated_account": "false"  
        })

        response = self.app.post('/api/auth/signup', headers={"Content-Type": "application/json"}, data=payload)

        #When
        payload = json.dumps(({
            "email": "mail@mail.com",
            "password": "mycoolpassword"
        }))

        response = self.app.post('/api/auth/login', headers={"Content-Type":"application/json"}, data=payload)

        self.assertEqual(str, type(response.json['token']))
        self.assertEqual(200, response.status_code)
