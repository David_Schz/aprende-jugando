import unittest
import json

from app import app
from tests.BaseCase import BaseCase

class SignupTest(BaseCase):


    def test_successful_signup(self):
        # Given
        payload = json.dumps({
            "nickname": "unit",
            "email": "mail@mail.com",
            "password": "mycoolpassword",
            "rol": "6096f7c88acf62d2c5e19f8a",
            "is_federated_account": "false"  
        })

        # When
        response = self.app.post('/api/auth/signup', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['id']))
        self.assertEqual(200, response.status_code)

